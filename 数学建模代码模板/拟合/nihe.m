clear;clc
x=[1,2,3,4,6,8,10,12];
y=[2,5,8,10,15,26,56,91];
plot(x,y,'o')
% 给x和y轴加上标签
xlabel('x的值')
ylabel('y的值')
n = size(x,1);
k = (n*sum(x.*y)-sum(x)*sum(y))/(n*sum(x.*x)-sum(x)*sum(x))
b = (sum(x.*x)*sum(y)-sum(x)*sum(x.*y))/(n*sum(x.*x)-sum(x)*sum(x))
hold on % 继续在之前的图形上来画图形
grid on % 显示网格线
 
% % 画出y=kx+b的函数图像 plot(x,y)
% % 传统的画法：模拟生成x和y的序列，比如要画出[0,5]上的图形
% x = 0: 0.1 :5  % 间隔设置的越小画出来的图形越准确
% y = k * x + b  % k和b都是已知值
% plot(x,y,'-')
 
f=@(x) k*ln(x)+b;
fplot(f,[min(x)-1,max(x)+1]);
legend('样本数据','拟合函数','location','SouthEast')
 
 
% 匿名函数的基本用法。
% handle = @(arglist) anonymous_function
% 其中handle为调用匿名函数时使用的名字。
% arglist为匿名函数的输入参数，可以是一个，也可以是多个，用逗号分隔。
% anonymous_function为匿名函数的表达式。
% 举个小例子
% % >> z=@(x,y) x^2+y^2; 
% % >> z(1,2) 
% % ans =  5
% fplot函数可用于画出匿名函数的图形。
% fplot(f,xinterval) 将在指定区间绘图。将区间指定为 [xmin xmax] 形式的二元素向量。
y_hat = k*x+b; % y的拟合值
SSR = sum((y_hat-mean(y)).^2)  % 回归平方和
SSE = sum((y_hat-y).^2) % 误差平方和
SST = sum((y-mean(y)).^2) % 总体平方和
SST-SSE-SSR
R_2 = SSR / SST

 
 
% % 注意：代码文件仅供参考，一定不要直接用于自己的数模论文中
% % 国赛对于论文的查重要求非常严格，代码雷同也算作抄袭
% % 更多优质数模资料可在我的微店获取：https://weidian.com/?userid=1372657210
% % 数学建模讨论群获取地址：http://note.youdao.com/noteshare?id=4997251d8219a45d56631e412b1e9392