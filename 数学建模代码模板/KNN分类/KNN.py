
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import pandas as pd

# 从 sklearn的datasets模块载入数据集加载酒的数据集
data1=pd.read_csv('winequality-red.csv', sep=';')##已知数据
data2=pd.read_csv('估价数据.csv', sep=';')##要进行预测的数据
x_train=data1.drop(["quality"],axis=1)
y_train=data1["quality"]
# 将数据集拆分为训练数据集和测试数据集
X_train,X_test,Y_train,Y_test=train_test_split(x_train,y_train,random_state=0)
knn = KNeighborsClassifier(n_neighbors=20)
knn.fit(X_train,Y_train)
print(knn)
# 评估模型的准确率
print('测试数据集得分：{:.2f}'.format(knn.score(X_test,Y_test)))
# 使用建好的模型对新酒进行分类预测
submission={"price":knn.predict(data2)}##进行预测价格
submission=pd.DataFrame(submission)
submission.to_csv('结果.csv')